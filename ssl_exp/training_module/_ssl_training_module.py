import logging
from typing import Any
from typing import Dict
from typing import List
from typing import Tuple

import pytorch_lightning as pl
import torch
import torch.nn.functional as F
import torch.optim as optim

from ssl_exp.models import SSLFeaturizerWithProjector


class SimCLRTrainingModule(pl.LightningModule):
    """A class that defines the SimCLR training module.

    Args:
        max_epochs: The maximum number of epochs.
        lr (optional): The learning rate. Defaults to 5e-4.
        temperature (optional): The temperature factor. Defaults to 0.07


    """

    def __init__(self, max_epochs: int, lr: float = 5e-4, temperature: float = 0.07) -> None:
        super().__init__()
        self._lr: float = lr
        self._max_epochs: int = max_epochs
        self._temperature: float = temperature
        self._model = SSLFeaturizerWithProjector(featurizer_output_dim=512, projection_output_dim=128)

    def configure_optimizers(self) -> Tuple[optim.Optimizer, List[Dict[str, Any]]]:
        """Configures the optimizer."""
        optimizer = optim.AdamW(self.parameters(), lr=self._lr)
        # See more info at https://hasty.ai/docs/mp-wiki/scheduler/cosineannealinglr
        lr_scheduler = optim.lr_scheduler.CosineAnnealingLR(
            optimizer=optimizer, T_max=self._max_epochs, eta_min=self._lr / 50
        )  # Because we use Pre-LN Transformer Encoder, warmup
        # is not needed.
        return [optimizer], [lr_scheduler]

    def training_step(self, batch: Tuple[List[torch.Tensor], torch.Tensor], batch_idx: int) -> torch.Tensor:
        self._calculate_loss(batch, mode="train")

    def _calculate_loss(self, batch: Tuple[List[torch.Tensor], torch.Tensor], mode: str) -> torch.Tensor:
        """Calculates the Info NCE loss.

        Args:
            batch: A tuple in which the first item is a list of batch image data for 2 views.
                The 2nd item is the label.
        """
        multiview_ims = batch[0]
        multiview_ims = torch.cat(multiview_ims, dim=0)  # 2 * B, C, H, W
        zs = self._model(multiview_ims)  # 2 * B, S

        # x1: 2*B, 1, S, x2: 1, 2*B, S. cos_sim[i, k] = cross_prod(x1[i, 1, :], x2[1, k, :])
        cos_sim = F.cosine_similarity(x1=zs[:, None, :], x2=zs[None, :, :], dim=-1)  # 2 * B, 2 * B
        cos_sim = cos_sim / self._temperature

        loss = -self._calculate_positive_cos_similarity(cos_sim) + self._calculate_log_sum_exp_cos_similarity(cos_sim)
        loss = loss.mean()
        self.log(f"{mode}_loss", loss)
        return loss

    @staticmethod
    def _calculate_positive_cos_similarity(sim: torch.Tensor) -> torch.Tensor:
        """Calculates the cosine similarity between augmented version of each samples.

        Args:
            sim: A 2B x 2B cosine similarity matrix where the augmented views are B rows apart to the diagonal.

        Returns:
            A length B list where the i-th item is sim(zi, zj) for each sample zi.
        """
        batch_size = sim.size(0) // 2
        return sim[
            torch.roll(torch.eye(n=2 * batch_size, dtype=torch.bool, device=sim.device), shifts=batch_size, dims=0)
        ]

    @staticmethod
    def _calculate_log_sum_exp_cos_similarity(sim: torch.Tensor) -> torch.Tensor:
        """Calculates log{sum_(k != i) {exp(sim(zi, zk))}} for the sample i-th.

        Args:
            sim: A 2B x 2B cosine similarity matrix where the augmented views are B rows apart to the diagonal.
        """
        batch_size = sim.size(0) // 2
        diag_mask = torch.eye(n=2 * batch_size, dtype=torch.bool, device=sim.device)
        exp_cos = torch.exp(sim)
        exp_cos[diag_mask] = 0.0
        return torch.log(torch.sum(exp_cos, dim=1))

    def validation_step(self, batch: Tuple[List[torch.Tensor], torch.Tensor], batch_idx: int) -> torch.Tensor:
        self._calculate_loss(batch, mode="val")
