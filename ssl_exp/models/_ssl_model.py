import torch
import torch.nn as nn
import torchvision


class SSLFeaturizerWithProjector(nn.Module):
    """A class that defines a featurizer in series with a projector.

    Args:
        featurizer_output_dim: The output dimension of the featurizer
        projection_output_dim: The output dimension of the projector
    """

    def __init__(self, featurizer_output_dim: int, projection_output_dim: int) -> None:
        super().__init__()
        self._model = torchvision.models.resnet18(num_classes=featurizer_output_dim)
        # Append the projection head. g(.)
        self._model.fc = nn.Sequential(
            self._model.fc,
            nn.ReLU(inplace=True),
            nn.Linear(in_features=featurizer_output_dim, out_features=projection_output_dim),
        )

    def __call__(self, x: torch.Tensor) -> torch.Tensor:
        return self._model(x)
