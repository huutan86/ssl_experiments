"""A package that performs the patch transformation."""
from ._multi_view_transformation import MultipleViewPatchTransformation
