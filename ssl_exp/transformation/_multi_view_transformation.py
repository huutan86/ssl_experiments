from typing import List

import torch
import torch.nn as nn


class MultipleViewPatchTransformation:
    """A class that returns the multiple views of the patch transformation.

    Args:
        transforms: A transformation module to be applied.
        n_views (optional): The number of views to return. Defaults to 2.
    """

    def __init__(self, transforms: nn.Module, n_views: int = 2) -> None:
        self._transforms = transforms
        self._n_views = n_views

    def __call__(self, x: torch.Tensor) -> List[torch.Tensor]:
        return [self._transforms(x) for _ in range(self._n_views)]
