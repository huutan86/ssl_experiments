import logging
from typing import List
from typing import Union

import matplotlib.pyplot as plt
import torch
import torchvision


def visualize_batch_images(ims: Union[List[torch.Tensor], torch.Tensor], nrows: int = 1) -> None:
    """Visualizes the images.

    Args:
        ims: A list of images to visualize. Each image has dimensions of [C, H, W].
        nrows (optional): The number of rows to display. Defaults to 1.
    """
    if isinstance(ims, list):
        ims = torch.stack(ims, dim=0)  # [B, C, H, W]
    im_grid = torchvision.utils.make_grid(ims, nrow=(ims.size(0) + 1) // nrows, normalize=True, pad_value=0.9).permute(
        1, 2, 0
    )
    plt.figure()
    plt.imshow(im_grid)
    plt.axis("off")
    plt.show()


def _get_training_devices() -> str:
    """Gets the training devices."""
    device = torch.device("gpu") if torch.cuda.is_available() else torch.device("cpu")
    logging.info(f"Device available {device}")
