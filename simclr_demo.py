"""A script that demonstrates SimCLR training on [STL10](https://cs.stanford.edu/~acoates/stl10/)."""
import logging
import os

import torch
from torchvision import transforms

logging.getLogger().setLevel(logging.INFO)
from torchvision.datasets import STL10, VisionDataset
from ssl_exp.transformation import MultipleViewPatchTransformation
from _utils import visualize_batch_images
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
import pytorch_lightning as pl
from _utils import _get_training_devices
from ssl_exp.training_module import SimCLRTrainingModule
from torch.utils.data import DataLoader


def _main() -> None:
    TARGET_SIZE_FOR_CROPPING_PIXELS = 96
    BATCH_SIZE = 16
    NUM_WORKERS = 1
    logging.info("Demoing the SimCLR algorithm on the STL10 dataset")
    DATASET_PATH = "./ssl_exp/datasets/data"
    multi_view_transform = MultipleViewPatchTransformation(
        transforms=transforms.Compose(
            [
                transforms.RandomHorizontalFlip(),
                transforms.RandomResizedCrop(size=TARGET_SIZE_FOR_CROPPING_PIXELS),
                transforms.RandomApply(
                    [transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5, hue=0.1)], p=0.8
                ),
                transforms.RandomGrayscale(p=0.2),
                transforms.GaussianBlur(kernel_size=9),
                transforms.ToTensor(),
                transforms.Normalize(mean=(0.5,), std=(0.5,)),
            ]
        ),
        n_views=2,
    )
    # Generate 45000 training images and 5000 validation images.
    unlabeled_dataset = STL10(root=DATASET_PATH, split="unlabeled", download=False, transform=multi_view_transform)
    train_dataset = STL10(root=DATASET_PATH, split="train", download=False, transform=multi_view_transform)
    # test_dataset = STL10(root=DATASET_PATH, split='test', download=True, transform=ims_transforms)
    logging.info(f"Num. of unlabeled images = {len(unlabeled_dataset)}.")
    logging.info(f"Num. of training images = {len(train_dataset)}.")

    _visualize_sample_images(dataset=train_dataset)
    unlabeled_dataloader = DataLoader(
        dataset=unlabeled_dataset,
        batch_size=BATCH_SIZE,
        drop_last=True,
        shuffle=True,
        pin_memory=True,
        num_workers=NUM_WORKERS,
    )
    val_dataloader = DataLoader(
        dataset=train_dataset, batch_size=BATCH_SIZE, shuffle=False, pin_memory=True, num_workers=NUM_WORKERS
    )
    _train_module(train_dataloader=unlabeled_dataloader, val_dataloader=val_dataloader)


def _visualize_sample_images(dataset: VisionDataset):
    LABEL_IDX = 0
    NUM_IMGS_TO_DISPLAY = 5
    NUM_VIEWS = 2
    # One row for each image.
    visualize_batch_images(
        ims=[dataset[idx][LABEL_IDX][view_idx] for view_idx in range(NUM_VIEWS) for idx in range(NUM_IMGS_TO_DISPLAY)],
        nrows=2,
    )


def _train_module(train_dataloader: DataLoader, val_dataloader: DataLoader) -> None:
    _CHECKPOINT_PATH = "~/Documents/ssl_exp"
    root_dir = os.path.join(_CHECKPOINT_PATH, "SimCLR")
    os.makedirs(root_dir, exist_ok=True)
    trainer = pl.Trainer(
        default_root_dir=root_dir,
        callbacks=[
            ModelCheckpoint(save_weights_only=True, mode="max", monitor="val_acc"),
            LearningRateMonitor("epoch"),
        ],
        accelerator=_get_training_devices(),
        devices=1,
        max_epochs=500,
    )

    training_module = SimCLRTrainingModule(max_epochs=500, lr=5e-4)

    trainer.fit(model=training_module, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader)


if __name__ == "__main__":
    _main()
