# ssl_experiments
This code repo is a collection of my code on ssl experiments.


## Getting started
If you have PathAI's built Docker image, you should be all set to run the experiment

## How the code is organized.

- `ssl_exp` A package that contains all necessary component for SSL training.
- `notebooks` A package that contains the notebooks of SimCLR training.

## How to install
```
cd existing_repo
git remote add origin https://gitlab.com/huutan86/ssl_experiments.git
git branch -M main
git push -uf origin main
```

## Reference
[1]. [Link](https://uvadlc-notebooks.readthedocs.io/en/latest/tutorial_notebooks/tutorial17/SimCLR.html) to tutorial.
[2]. [Colab](https://colab.research.google.com/github/phlippe/uvadlc_notebooks/blob/master/docs/tutorial_notebooks/tutorial17/SimCLR.ipynb) notebook for the SimCLR tutorial.
[3]. [SimCLR](https://arxiv.org/abs/2006.10029) paper
[4]. [SimCLRv2](https://arxiv.org/abs/2006.10029) paper i.e. "Big Self-Supervised Models are Strong Semi-Supervised Learners"
[5]. [InfoNCE loss paper](https://arxiv.org/abs/1807.03748)
